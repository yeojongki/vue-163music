import request from 'utils/request'

export default {
  getPlayList(limit) {
    return request({
      url: `top/playlist/highquality?limit=${limit}`
    })
  },
  getPlayListDetail (id) {
    return request({
      url: `playlist/detail?id=${id}`
    })
  },
  getMvList(id) {
    return request({
      url: `personalized/mv`
    })
  },
  getSong(id) {
    return request({
      url: `music/url?id=${id}`
    })
  },
  getBanner() {
    return request({
      url: `banner`
    })
  },
  getLyric(id) {
    return request({
      url: `lyric?id=${id}`
    })
  }
}

// const _baseUrl2 = 'https://api.imjad.cn/cloudmusic'
// export default {
//   getPlayListByWhere (cat, order, offset, total, limit) {
//     return _baseUrl + '?type=topPlayList&cat=' + cat + '&offset=' + offset + '&limit=' + limit
//     //http://music.163.com/api/playlist/list?cat={}&order={}&offset={}&total={}&limit={}
//   },
//   getLrc (id) {
//     return _baseUrl2 + '?type=lyric&id=' + id
//   },
//   getSong (id) {
//     return _baseUrl + '?type=url&id=' + id
//   },
//   getPlayListDetail (id) {
//     return _baseUrl2 + '?type=playlist&id=' + id
//   },
//   getMv (id) {
//     return _baseUrl2 + '?type=mv&id=' + id
//   },
//   search (words) {
//     return _baseUrl2 + '?type=search&s=' + words
//   }
// }
