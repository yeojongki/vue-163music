import Vue from 'vue'
import Vuex from 'vuex'
import api from '../api'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    audio: {
      'id': 0,
      'name': '歌曲名称',
      'singer': '演唱者',
      'albumPic': '/static/player-bar.png',
      'location': '',
      'album': ''
    },
    lyric: '正在加载中',
    currentIndex: 0, // 当前播放的歌曲位置
    playing: false, // 是否正在播放
    loading: false, // 是否正在加载中
    songList: [],    // 播放列表
    currentTime: 0,
    tmpCurrentTime: 0,
    durationTime: 0,
    bufferedTime: 0,
    change: false,   // 判断是更改的时间还是播放的时间
    showDetail: false
  },
  getters: {
    audio: state => state.audio,
    playing: state => state.playing,
    loading: state => state.loading,
    currentIndex: state=> state.currentIndex,
    songList: state => state.songList,
    currentTime: state => state.currentTime,
    change: state => state.change,
    tmpCurrentTime: state => state.tmpCurrentTime,
    prCurrentTime: state => {
      return state.currentTime / state.durationTime * 100
    },
    prBufferedTime: state => {
      return state.bufferedTime / state.bufferedTime * 100
    },
    showDetail: state => state.showDetail,
    durationTime: state => state.durationTime,
  },
  mutations: {
    openLoading (state) {
      state.loading = true
    },
    closeLoading (state) {
      state.loading = false
    },
    play (state) {
      state.playing = true
    },
    pause (state) {
      state.playing = false
    },
    playNext (state) {
      state.currentIndex++
      if (state.currentIndex > state.songList.length) {
        state.currentIndex = 1
      }
      state.audio = state.songList[state.currentIndex - 1]
    },
    setLocation (state, location) {
      state.audio.location = location
    },
    addToList (state, item) {
      var flag = false
      state.songList.forEach(function (element, index) { // 检测歌曲重复
        if (element.id === item.id) {
          flag = true
          state.currentIndex = index + 1
        }
      })
      if (!flag) {
        state.songList.push(item)//在列表中添加传入过来的点击的每一首歌
        state.currentIndex = state.songList.length//列表歌曲数量
      }
    },
    setAudio (state) {
      state.audio = state.songList[state.currentIndex - 1]
    },
    /************更新播放进度条****************/
    updateBufferedTime (state,time) {
      state.bufferedTime = time
    },
    updateDurationTime (state, time) {
      state.durationTime = time
    },
    setChange (state, flag) {
      state.change = flag
    },
    updateCurrentTime (state, time) {
      state.currentTime = time
    },
    /***********bottomList**************/
    setAudioIndex (state, index) {
      state.audio = state.songList[index]
      state.currentIndex = index + 1
    },
    removeAudio (state, index) {
      state.songList.splice(index, 1)
      //删除从Index开始后(包括index)的1首歌
      if (index === state.songList.length) {
        index--
      }
      state.audio = state.songList[index]
      state.currentIndex = index + 1
      if (state.songList.length === 0) {
        state.audio = {
          'id': 0,
          'name': '歌曲名称',
          'singer': '演唱者',
          'albumPic': '/static/player-bar.png',
          'location': '',
          'album': ''
        }
        state.playing = false
      }
    },
    toggleDetail (state) {
      state.showDetail = !state.showDetail
    },
    /**************playerDetail*********************/
    playNext (state) { // 播放下一曲
      state.currentIndex++
      if (state.currentIndex > state.songList.length) {
        state.currentIndex = 1
      }
      state.audio = state.songList[state.currentIndex - 1]
    },
    playPrev (state) { // 播放上一曲
      state.currentIndex--
      if (state.currentIndex < 1) {
        state.currentIndex = state.songList.length
      }
      state.audio = state.songList[state.currentIndex - 1]
    },
    changeTime (state, time) {
      state.tmpCurrentTime = time
    },
  },
  actions: {
    getSong ({commit, state}, id) {
      commit('openLoading')
      api.getSong(id).then(res => {
        // 统一数据模型，方便后台接口的改变
        var url = res.data.data[0].url
        commit('setAudio')
        commit('setLocation', url)
      })
    }
  }
})
export default store
