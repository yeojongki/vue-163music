import Vue from 'vue'
import Router from 'vue-router'

// import index from 'views/index'
// import rage from 'views/rage'
// import songList from 'views/songList'
// import leaderBoard from 'views/leaderBoard'
// import hotSinger from 'views/hotSinger'
// import playListDetail from 'views/playListDetail'
// import playerDetail from 'views/playerDetail'

const index = require('views/index')
const rage = require('views/rage')
const songList = require('views/songList')
const leaderBoard = require('views/leaderBoard')
const hotSinger = require('views/hotSinger')
const playListDetail = require('views/playListDetail')
const playerDetail = require('views/playerDetail')

// const rage = resolve => require(['views/rage'],resolve)
// const songList = resolve => require(['views/songList'],resolve)
// const leaderBoard = resolve => require(['views/leaderBoard'],resolve)
// const hotSinger = resolve => require(['views/hotSinger'],resolve)
// const playListDetail = resolve => require(['views/playListDetail'],resolve)
// const playerDetail = resolve => require(['views/playerDetail'],resolve)

// const index = resolve => require(['views/index'],resolve)
// const rage = resolve => require(['views/rage'],resolve)
// const songList = resolve => require(['views/songList'],resolve)
// const leaderBoard = resolve => require(['views/leaderBoard'],resolve)
// const hotSinger = resolve => require(['views/hotSinger'],resolve)
// const playListDetail = resolve => require(['views/playListDetail'],resolve)
// const playerDetail = resolve => require(['views/playerDetail'],resolve)

Vue.use(Router)
const router = new Router({
	mode:'history',
  routes: [
    { 
    	path: '/',
    	component: index,
      children: [
	      {
	        path: 'rage',
	        component: rage
	      },
	      {
	        path: 'songList',
	        component: songList
	      },
	      {
	        path: 'leaderBoard',
	        component: leaderBoard
	      },
	      {
	        path: 'hotSinger',
	        component: hotSinger
	      }
    	]
    },
    {
    	name:'playListDetail',
	    path: '/playListDetail/:id',
	    component: playListDetail
  	},
  	{
  		name: 'playerDetail',
	    path: '/playerDetail/:id',
	    component: playerDetail
  	},
    {
			path:'*',
    	redirect:'/'
    }	
  ]
})

export default router