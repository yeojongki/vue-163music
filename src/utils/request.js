import axios from 'axios';
const instance = axios.create({
  baseURL: 'http://api.yeojongki.cn/',
  timeout: 10000
});
// code状态码200判断
instance.interceptors.response.use(res => {
  if (res.data.code !== 200) {
    return Promise.reject(res);
  }
  return res;
}, error => {
  return Promise.reject(error);
})
export default instance;